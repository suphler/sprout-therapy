import {Custom1} from './custom1';
import {MPTEnum} from './enum/mtp.enum';
import {CustomKParam} from './interfaces/customkparam';

export class Custom2H extends Custom1 {

    constructor(a, b, c, d, e, f) {
        super(a, b, c, d, e, f);
        const CKP2 =
            [{
                 H: MPTEnum.M,
                 K(a, b, c, d, e, f): number {
                     return f + d + (d * e / 100);
                 }
             } as CustomKParam];
        this.CKP = [...this.CKP, ...CKP2];
    }

    getH(): MPTEnum {
        if (this.isCheck4()) {
            return this.h = MPTEnum.T;
        }
        if (this.isCheck5()) {
            return this.h = MPTEnum.M;
        }
        if (this.h !== undefined) {
            return this.h;
        }
        if (this.h === undefined) {
            return super.getH();
        }

    }

    isCheck4(): boolean {
        return this.a && this.b && !this.c;
    }

    isCheck5(): boolean {
        return this.a && !this.b && this.c;
    }

    getCustomKParam(): CustomKParam[] {
        return this.CKP;
    } ;
}

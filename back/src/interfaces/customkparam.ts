import {MPTEnum} from '../enum/mtp.enum';

export interface CustomKParam {
    H: MPTEnum;

    K(a, b, c, d, e, f): number;
}

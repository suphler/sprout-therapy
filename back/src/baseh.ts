import {H} from './h';
import {MPTEnum} from './enum/mtp.enum';

export class BaseH extends H {
    a;
    b;
    c;
    d;
    e;
    f;

    constructor(a: boolean, b: boolean, c: boolean, d: number, e: number, f: number) {
        super();
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
        this.e = e;
        this.f = f;
    }

    getH() {
        if (this.isCheck1()) {
            return this.h = MPTEnum.M;
        }
        if (this.isCheck2()) {
            return this.h = MPTEnum.P;
        }
        if (this.isCheck3()) {
            return this.h = MPTEnum.T;
        }
        if (this.h === undefined) {
            throw new Error('Error input params');
        }

    }

    isCheck1(): boolean {
        return this.a && this.b && !this.c;
    }

    isCheck2(): boolean {
        return this.a && this.b && this.c;
    }

    isCheck3(): boolean {
        return !this.a && this.b && this.c;
    }
}

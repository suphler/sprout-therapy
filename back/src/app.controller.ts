import {Body, Controller, Get, Post, Req} from '@nestjs/common';
import { AppService } from './app.service';
import {InputDataDto} from './interfaces/inputdata.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}


  @Post('/inputdata/')
  async getHAndK(@Body() inputDataDTO: InputDataDto, @Req() request) {
    console.log('inputDataDTO: ', inputDataDTO);

    const ans = await this.appService.getHAndK(inputDataDTO);
    return ans;
  }
}

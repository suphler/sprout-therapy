import {Injectable} from '@nestjs/common';
import {InputDataDto} from './interfaces/inputdata.dto';
import {BaseK} from './basek';
import {Custom2H} from './custom2h';

@Injectable()
export class AppService {
    getHAndK(inputDataDTO: InputDataDto) {
        const k = new BaseK(new Custom2H(inputDataDTO.a, inputDataDTO.b, inputDataDTO.c, inputDataDTO.d, inputDataDTO.e, inputDataDTO.f));
        console.log('H: ', k.getH());
        console.log('K: ', k.getK());
        return {H: k.getH(), K: k.getK()}
    }
}

import {BaseH} from './baseh';
import {MPTEnum} from './enum/mtp.enum';
import {CustomKParam} from './interfaces/customkparam';

export class Custom1 extends BaseH {

    constructor(a, b, c, d, e, f) {
        super(a, b, c, d, e, f);
        this.CKP = [
            {
                H: MPTEnum.P,
                K(a, b, c, d, e, f): number {
                    return 2 * d + (d * e / 100);
                }
            } as CustomKParam
        ];
    }

    getH(): MPTEnum {
        return super.getH();
    };

    getCustomKParam(): CustomKParam[] {
        return this.CKP;
    } ;

}

import {MPTEnum} from './enum/mtp.enum';
import {CustomKParam} from './interfaces/customkparam';

export abstract class H {
    h: MPTEnum;
    CKP: CustomKParam[];

    getH() {
    }

    getCustomKParam(): CustomKParam[] {
        return this.CKP;
    };
}

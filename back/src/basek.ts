import {K} from './k';
import {Custom2H} from './custom2h';
import {H} from './h';
import {MPTEnum} from './enum/mtp.enum';

export class BaseK extends K {
    h: Custom2H;
    tmpK: number;

    constructor(h: Custom2H) {
        super(h);
        this.h = h;
    }

    getK() {
        if (this.isHM()) {
            this.tmpK = this.h.d + (this.h.d * this.h.e / 10);
        }
        if (this.isHP()) {
            this.tmpK = this.h.d + (this.h.d * (this.h.e - this.h.f) / 25.5);
        }
        if (this.isHT()) {
            this.tmpK = this.h.d + (this.h.d * this.h.f / 30);
        }


        this.applyCustom();
        return this.tmpK;
    }

    getH(){
        return this.h.getH();
    }

    applyCustom() {
        const customParams = this.h.getCustomKParam();
        customParams.forEach((param) => {
            if (param.H === this.h.getH()) {
            this.tmpK = param.K(this.h.a, this.h.b, this.h.c, this.h.d, this.h.e, this.h.f );
        }
        });

    }

    isHM(customH?: H) {
        return customH || this.h.getH() === MPTEnum.M;
    }

    isHP(customH?: H) {
        return customH || this.h.getH() === MPTEnum.P;
    }

    isHT(customH?: H) {
        return customH || this.h.getH() === MPTEnum.T;
    }
}

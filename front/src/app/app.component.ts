import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AppService} from './app.service';
import {HandK} from './handk';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'front';
  inputDataForm: FormGroup;
  handk: Observable<HandK>;

  constructor(private appService: AppService){

  }

  ngOnInit(): void {
    this.initInputForm();
  }

  initInputForm(){
    this.inputDataForm = new FormGroup({
      a: new FormControl(true),
      b: new FormControl(true),
      c: new FormControl(true),
      d: new FormControl('0', Validators.required),
      e: new FormControl('0', Validators.required),
      f: new FormControl('0', Validators.required),
    });

  }

  onSubmit(){
console.log('submit', this.inputDataForm.value);
if (this.inputDataForm.valid) {
this.handk = this.appService.getHAndKFromAPI(this.inputDataForm.value);
}
  }

}

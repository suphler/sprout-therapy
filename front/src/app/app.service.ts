import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../environments/environment';
import {HandK} from './handk';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  private apiURL = environment.apiURL;

  constructor(private httpClient: HttpClient) {
  }

  getHAndKFromAPI(payload) {
    console.log('PAYLOAD:', payload);
    return this.httpClient.post<HandK>(this.apiURL + '/inputdata/', payload);

  }
}
